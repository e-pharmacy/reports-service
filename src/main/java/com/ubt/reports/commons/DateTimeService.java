package com.ubt.reports.commons;

import com.ubt.pos.exceptions.BadRequestException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeService {
  public static Long toLong(LocalDateTime localDateTime) {
    if (localDateTime != null) {
      return localDateTime.atZone(ZoneId.systemDefault())
              .toInstant().toEpochMilli();
    }
    return null;
  }

  public static LocalDateTime toLocalDateTime(Long timestamp) {
    if (timestamp != null) {
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp),
              TimeZone.getDefault().toZoneId());
    }
    return null;
  }

  /**
   * Converts "yyyy-MM-dd hh:mm:ss" format to Long timestamp value.
   *
   * @param dateTime
   * @return
   */
  public static Long dateStringFormatToLong(String dateTime) {
    if (dateTime != null) {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

      Date parsedDate;
      try {
        parsedDate = dateFormat.parse(dateTime);
      } catch (ParseException parseException) {
        throw new BadRequestException("Invalid date format!");
      }

      return parsedDate.getTime();
    }
    return null;
  }

//  /**
//   * Accepts two localDateTime objects and subtracts the @localDateTime2 from @LocalDateTime1.
//   * Returns the difference between two localDateTime objects as long.
//   *
//   * @param localDateTime1
//   * @param localDateTime2 the value that is subtracted from localDateTime1
//   * @return
//   */
//  public static Long subtractLocalDateTime(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
//    //calculate time left (difference between the time that test must be finished and the current time)
//    LocalDateTime timeLeft = localDateTime1.minusHours(localDateTime2.getHour());
//    timeLeft = timeLeft.minusMinutes(localDateTime2.getMinute());
//    timeLeft = timeLeft.minusSeconds(localDateTime2.getSecond());
//    Timestamp timeDifferenceTimestamp = Timestamp.valueOf(timeLeft);
//
//    //create a timestamp that has only hours, minutes and seconds to tell the time left.
//    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
//    String formattedTime = sdf.format(timeDifferenceTimestamp.getTime());
//    DateFormat df = new SimpleDateFormat("HH:mm:ss");
//    Date time = null;
//    try {
//      time = df.parse(formattedTime);
//    } catch (Exception e) {
//      return null;
//    }
//    return time.getTime();
//  }
}
