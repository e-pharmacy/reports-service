package com.ubt.reports.clients;

import com.ubt.pos.commons.models.BranchModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(value = "stock-service", url = "${feign.address.stock-service}")
public interface StockFeignClient {
  @GetMapping("")
  List<BranchModel> getBranches(@RequestParam List<Long> ids);
}
