package com.ubt.reports.clients;

import com.ubt.pos.sales.models.ProductModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(value = "medicine-service", url = "${feign.address.medicine-service}")
public interface MedicineFeignClient {
  @GetMapping("/products")
  List<ProductModel> getProducts(@RequestParam List<Long> ids, @RequestParam(required = false) List<String> expand, @RequestParam Boolean unPaged);
}
