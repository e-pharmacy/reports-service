package com.ubt.reports.clients;

import com.ubt.reports.commons.models.BranchModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(value = "pos-service", url = "${feign.address.pos-service}")
public interface PosFeignClient {
  @GetMapping("")
  List<BranchModel> getBranches(@RequestParam List<Long> ids);
}
