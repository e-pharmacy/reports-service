package com.ubt.reports.utils;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FeignTokenInterceptor implements RequestInterceptor {
  private static final String AUTHORIZATION = "Authorization";
  @Value("${service-token}")
  public String serviceToken;

  @Override
  public void apply(RequestTemplate requestTemplate) {
    requestTemplate.header("service-token", serviceToken);

    if (UserContextHolder.getContext().getAuthToken() == null) {

      return;
    }

    requestTemplate.header(AUTHORIZATION, UserContextHolder.getContext().getAuthToken());
  }
}