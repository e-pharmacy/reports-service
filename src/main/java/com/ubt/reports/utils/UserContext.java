package com.ubt.reports.utils;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserContext {
  private String authToken;
  private List<Long> userRoles;
  private Long userId;
  private String userEmail;
  private Long locationId;
  private String firstName;
  private String lastName;

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String aToken) {
    authToken = aToken;
  }

  public List<Long> getUserRoles() {
    return userRoles;
  }

  public void setUserRoles(List<Long> userRoles) {
    this.userRoles = userRoles;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String email) {
    userEmail = email;
  }

  public Long getLocationId() {
    return locationId;
  }

  public void setLocationId(long locationId) {
    this.locationId = locationId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}