package com.ubt.reports.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class UserContextFilter implements Filter {
  public static final String AUTH_TOKEN = "Authorization";
  public static final String ROLES = "authorities";
  public static final String ID = "id";
  public static final String LOCATION_ID = "location_id";
  public static final String EMAIL = "email";
  private static final String FIRST_NAME = "first_name";
  private static final String LAST_NAME = "last_name";

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
          throws IOException, ServletException {

    HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

    String token = httpServletRequest.getHeader(AUTH_TOKEN);

    extractUserFromToken(token);

    filterChain.doFilter(servletRequest, servletResponse);
  }

  private void extractUserFromToken(String auth) {

    if (auth == null) {
      return;
    }

    UserContext userContext = UserContextHolder.getContext();
    userContext.setAuthToken(auth);

    String authToken = auth.replace("Bearer ", "");

    try {
      Jwt jwtToken = JwtHelper.decode(authToken);
      String claims = jwtToken.getClaims();

      HashMap claimsMap = null;
      try {
        claimsMap = new ObjectMapper().readValue(claims, HashMap.class);
      } catch (IOException e) {
        e.printStackTrace();
      }

      List<String> userRoles;
      String userEmail;
      Integer userId;
      Integer locationId;
      String firstName;
      String lastName;

      if (claimsMap != null) {
        userRoles = (List<String>) claimsMap.get(ROLES);
        userId = (Integer) claimsMap.get(ID);
        locationId = (Integer) claimsMap.get(LOCATION_ID);
        userEmail = (String) claimsMap.get(EMAIL);
        firstName = (String) claimsMap.get(FIRST_NAME);
        lastName = (String) claimsMap.get(LAST_NAME);

        // Set user id on userContext
        userContext.setUserId(userId);
        // Set user location id on userContext
        userContext.setLocationId(locationId);
        // Set user email on userContext
        userContext.setUserEmail(userEmail);
        // Set user first name on userContext
        userContext.setFirstName(firstName);
        // Set user last name on userContext
        userContext.setLastName(lastName);

        //Set roles empty
        userContext.setUserRoles(new ArrayList<>());

        if (!CollectionUtils.isEmpty(userRoles)) {
          // Set user roles Ids on userContext
          userContext.setUserRoles(getUserRolesIds(userRoles));

          updateSecurityContext(userRoles);
        }
      }
    } catch (Exception e) {
      logger.error("Token expired.");
    }
  }

  private List<Long> getUserRolesIds(List<String> userRoles) {
    List<Long> userRolesIds = new ArrayList<>();

    for (String userRole : userRoles) {
      switch (userRole.toLowerCase()) {
        case "admin":
          userRolesIds.add(1L);
          break;
        case "user":
          userRolesIds.add(2L);
          break;
      }
    }

    return userRolesIds;
  }

  @Override
  public void init(FilterConfig filterConfig) {
  }

  @Override
  public void destroy() {
  }

  private void updateSecurityContext(List<String> userRoles) {

    // Update security context
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    List<GrantedAuthority> updatedAuthorities = new ArrayList<>(auth.getAuthorities());

    for (String role : userRoles) {
      updatedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + role));
    }

    Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), updatedAuthorities);

    SecurityContextHolder.getContext().setAuthentication(newAuth);
  }
}