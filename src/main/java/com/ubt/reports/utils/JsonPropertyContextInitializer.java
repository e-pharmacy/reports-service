package com.ubt.reports.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class JsonPropertyContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Override
  public void initialize(ConfigurableApplicationContext configurableApplicationContext) {

    String configPath = System.getenv("CONFIG_FILE");

    // For development purposes when the config path is not provided on runtime
    // it will use a default config.json file
    if (configPath == null) {

      logger.info("Dev config will be loaded.");
      configPath = "classpath:config.json";
    } else {

      logger.info("Production config will be loaded.");
      configPath = "file:" + configPath;
    }

    try {
      Resource resource = configurableApplicationContext.getResource(configPath);
      Map readValue = new ObjectMapper().readValue(resource.getInputStream(), Map.class);
      Set<Map.Entry> set = readValue.entrySet();
      List<MapPropertySource> propertySources = convertEntrySet(set, Optional.empty());
      for (PropertySource propertySource : propertySources) {
        configurableApplicationContext.getEnvironment()
                .getPropertySources()
                .addFirst(propertySource);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static List<MapPropertySource> convertEntrySet(Set<Map.Entry> entrySet, Optional<String> parentKey) {
    return entrySet.stream()
            .map((Map.Entry e) -> convertToPropertySourceList(e, parentKey))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
  }

  private static List<MapPropertySource> convertToPropertySourceList(Map.Entry e, Optional<String> parentKey) {
    String key = parentKey.map(s -> s + ".")
            .orElse("") + (String) e.getKey();
    Object value = e.getValue();
    return covertToPropertySourceList(key, value);
  }

  private static List<MapPropertySource> covertToPropertySourceList(String key, Object value) {
    if (value instanceof LinkedHashMap) {
      LinkedHashMap map = (LinkedHashMap) value;
      Set<Map.Entry> entrySet = map.entrySet();
      return convertEntrySet(entrySet, Optional.ofNullable(key));
    }
    return Collections.singletonList(
            new MapPropertySource(key,
                    Collections.singletonMap(key, value)));
  }
}