/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 21/09/2021.
 */

package com.ubt.reports.sales;

import com.ubt.reports.sales.models.MonthlySaleReportsModel;
import com.ubt.reports.sales.models.SaleReportsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sales")
public class SalesController {
  private SalesService salesService;
}
