/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 21/09/2021.
 */

package com.ubt.reports.sales.models;

import com.ubt.reports.commons.models.BoBaseModel;

public class MonthlySaleReportsModel extends BoBaseModel {
  public Integer salesCount;
  private String month;

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public Integer getSalesCount() {
    return salesCount;
  }

  public void setSalesCount(Integer salesCount) {
    this.salesCount = salesCount;
  }
}
